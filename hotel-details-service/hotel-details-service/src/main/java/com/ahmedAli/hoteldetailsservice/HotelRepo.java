package com.ahmedAli.hoteldetailsservice;

import org.springframework.data.repository.CrudRepository;
import org.springframework.stereotype.Repository;

import com.ahmedAli.hoteldetailsservice.model.Hotel;
@Repository
public interface HotelRepo extends CrudRepository<Hotel, Integer> {

}
