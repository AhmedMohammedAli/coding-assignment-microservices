package com.ahmedAli.hoteldetailsservice.model;

import java.util.List;
public class HotelList {
	List<Hotel> hotelsList;
	public HotelList() {
		// TODO Auto-generated constructor stub
	}

	public List<Hotel> getHotelsList() {
		return hotelsList;
	}
	public void setHotelsList(List<Hotel> hotelsList) {
		this.hotelsList = hotelsList;
	}

	@Override
	public String toString() {
		return "HotelList [hotelsList=" + hotelsList + "]";
	}

}
