package com.ahmedAli.hoteldetailsservice;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RestController;

import com.ahmedAli.hoteldetailsservice.model.Hotel;
import com.ahmedAli.hoteldetailsservice.model.HotelList;

@RestController
@RequestMapping("/hotels")
public class HotelService {
	HotelList hotelList = new HotelList();
	@Autowired
	HotelRepo hotelRepo;

	@RequestMapping(value = "/addHotel", method = RequestMethod.POST)
	public String addHotel(@RequestBody Hotel hotel) {
		hotelRepo.save(hotel);
		return "hotel Added";
	}

	@RequestMapping("/getAllHotels")
	public HotelList getAllHotels() {
		List<Hotel> list = (List<Hotel>) getCollectionFromIteralbe(hotelRepo.findAll());
		hotelList.setHotelsList(list);
		return hotelList;
	}

	public static <T> Collection<T> getCollectionFromIteralbe(Iterable<T> itr) {
// Create an empty Collection to hold the result 
		Collection<T> cltn = new ArrayList<T>();

// Iterate through the iterable to 
// add each element into the collection 
		for (T t : itr)
			cltn.add(t);

// Return the converted collection 
		return cltn;
	}
}
