/**
 * 
 */
package com.ahmedAli.hoteldetailsservice.model;
import javax.persistence.Entity;
import javax.persistence.Id;
/**
 * @author Ahmed Mohamed Ali
 *
 */
@Entity
public class Hotel {

	/**
	 * 
	 */
	@Id
	Integer hotelId;
	
	String hotelName;
	int userId;
	int rating;
	String userName;
	public Hotel() {
		 
	}
	public Hotel(Integer hotelId, String hotelName, int userId, int rating, String userName) {
		super();
		this.hotelId = hotelId;
		this.hotelName = hotelName;
		this.userId = userId;
		this.rating = rating;
		this.userName = userName;
	}


	
	
	public Integer getHotelId() {
		return hotelId;
	}




	public void setHotelId(Integer hotelId) {
		this.hotelId = hotelId;
	}




	public String getHotelName() {
		return hotelName;
	}




	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}




	public int getUserId() {
		return userId;
	}




	public void setUserId(int userId) {
		this.userId = userId;
	}




	public int getRating() {
		return rating;
	}




	public void setRating(int rating) {
		this.rating = rating;
	}




	public String getUserName() {
		return userName;
	}




	public void setUserName(String userName) {
		this.userName = userName;
	}




	
	
	

}
