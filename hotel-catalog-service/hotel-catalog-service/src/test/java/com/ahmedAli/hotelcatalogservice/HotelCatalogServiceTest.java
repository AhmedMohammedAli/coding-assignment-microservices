/**
 * 
 */
package com.ahmedAli.hotelcatalogservice;

import static org.junit.Assert.*;

import java.net.URI;
import java.net.URISyntaxException;

import org.junit.Assert;
import org.junit.Test;
import org.springframework.http.ResponseEntity;
import org.springframework.web.client.RestTemplate;

import com.ahmedAli.model.HotelList;

/**
 * @author Ahmed Mohamed Ali
 *
 */
public class HotelCatalogServiceTest {
 

	/**
	 * Test method for {@link com.ahmedAli.hotelcatalogservice.HotelCatalogService#getAllHotels()}.
	 * @throws URISyntaxException 
	 */
	@Test
	public final void testGetAllHotels() throws URISyntaxException {
		 RestTemplate restTemplate = new RestTemplate();
	     
		    final String baseUrl = "http://localhost:8090/hotels/getAllHotels";
		    URI uri = new URI(baseUrl);
		 
		    ResponseEntity<String> result = restTemplate.getForEntity(uri, String.class);
		     
		    //Verify request succeed
		    Assert.assertEquals(200, result.getStatusCodeValue());
		    Assert.assertEquals(true, result.getBody().contains("hotelsList"));
	}

}
