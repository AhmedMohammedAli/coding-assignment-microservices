/**
 * 
 */
package com.ahmedAli.model;

import org.hibernate.validator.constraints.NotBlank;

/**
 * @author Ahmed Mohamed Ali
 *
 */
public class Hotel {

	/**
	 * 
	 */
	Integer hotelId;
	 @NotBlank(message = "hotel Name is mandatory")
	String hotelName;
	int userId;
	int rating;
	 @NotBlank(message = "User Name is mandatory")
	String userName;
	public Hotel() {
		 
	}
	public Hotel(Integer hotelId, String hotelName, int userId, int rating, String userName) {
		super();
		this.hotelId = hotelId;
		this.hotelName = hotelName;
		this.userId = userId;
		this.rating = rating;
		this.userName = userName;
	}


	
	
	public Integer getHotelId() {
		return hotelId;
	}




	public void setHotelId(Integer hotelId) {
		this.hotelId = hotelId;
	}




	public String getHotelName() {
		return hotelName;
	}




	public void setHotelName(String hotelName) {
		this.hotelName = hotelName;
	}




	public int getUserId() {
		return userId;
	}




	public void setUserId(int userId) {
		this.userId = userId;
	}




	public int getRating() {
		return rating;
	}




	public void setRating(int rating) {
		this.rating = rating;
	}




	public String getUserName() {
		return userName;
	}




	public void setUserName(String userName) {
		this.userName = userName;
	}




	
	
	

}
