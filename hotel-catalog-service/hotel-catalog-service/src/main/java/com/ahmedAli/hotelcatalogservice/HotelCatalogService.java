/**
 * 
 */
package com.ahmedAli.hotelcatalogservice;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpHeaders;
import org.springframework.http.HttpMethod;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.client.RestTemplate;

import com.ahmedAli.model.Hotel;
import com.ahmedAli.model.HotelList;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import javax.validation.Valid;

/**
 * @author Ahmed Mohamed Ali
 *
 */
@RestController
@RequestMapping("/hotelCatalog")
public class HotelCatalogService {
	private static final Logger logger = LoggerFactory.getLogger(HotelCatalogService.class);
	@Autowired
	RestTemplate restTemplate;

	// this service is used to add hotel rating with user details in h2 DB
	@RequestMapping(value = "/addHotel", method = RequestMethod.POST)
	@HystrixCommand(fallbackMethod = "addHotelFallback")
	public ResponseEntity<String> addHotel(@Valid @RequestBody Hotel hotel) {
		logger.info("start calling add hotel service");
		HttpHeaders requestHeaders = new HttpHeaders();
		requestHeaders.setContentType(MediaType.APPLICATION_JSON);
		requestHeaders.setAccept(Arrays.asList(MediaType.APPLICATION_JSON));

		// request entity is created with request body and headers
		HttpEntity<Hotel> requestEntity = new HttpEntity<>(hotel, requestHeaders);

		ResponseEntity<String> responseEntity = restTemplate.exchange("http://HOTEL-DETAILS-SERVICE/hotels/addHotel",
				HttpMethod.POST, requestEntity, String.class);
		String reply = "user not added";
		if (responseEntity.getStatusCode() == HttpStatus.OK) {
			reply = responseEntity.getBody();
			logger.info("user response retrieved");

		}
		return new ResponseEntity<String>(HttpStatus.OK);

	}
	public ResponseEntity<String> addHotelFallback(@Valid @RequestBody Hotel hotel) {
		
		return new ResponseEntity<String>(HttpStatus.OK);
	}
	 //this service is used for custome exception handling and for handling validating hotel attributes
	@ResponseStatus(HttpStatus.BAD_REQUEST)
	@ExceptionHandler(MethodArgumentNotValidException.class)
	public Map<String, String> handleValidationExceptions(MethodArgumentNotValidException ex) {

		Map<String, String> errors = new HashMap<>();
		ex.getBindingResult().getAllErrors().forEach((error) -> {
			String fieldName = ((FieldError) error).getField();
			String errorMessage = error.getDefaultMessage();
			errors.put(fieldName, errorMessage);
		});
		return errors;
	}
	
	 //this service is used get all hotels rating  with user details in h2 DB
	@RequestMapping("/getAllHotels")
	@HystrixCommand(fallbackMethod = "getAllHotelsFallback")
	public HotelList getAllHotels() {
		logger.info("start calling   getAllHotels service");
		HotelList result = restTemplate.getForObject("http://HOTEL-DETAILS-SERVICE/hotels/getAllHotels",
				HotelList.class);
		logger.info("end calling   getAllHotels service the result is :" + result);
		return result;
	}
	public HotelList getAllHotelsFallback() {
		
		
		HotelList result =new HotelList();
		List<Hotel> list=new ArrayList<Hotel>();
		list.add(new Hotel(0,"no hotels found",0,0,"no user"));
		result.setHotelsList(list );
		return result;
	}

}
