package com.ahmedAli.hotelcatalogservice;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.client.circuitbreaker.EnableCircuitBreaker;
import org.springframework.cloud.client.loadbalancer.LoadBalanced;
import org.springframework.cloud.netflix.hystrix.dashboard.EnableHystrixDashboard;
import org.springframework.context.annotation.Bean;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableCircuitBreaker
@EnableHystrixDashboard
public class HotelCatalogServiceApplication {
@Bean
@LoadBalanced
public RestTemplate getRestTemplate() {
	
	return new RestTemplate();
}
	public static void main(String[] args) {
		SpringApplication.run(HotelCatalogServiceApplication.class, args);
	}

}
