'use strict'

 
ratingApp.controller("ratingController", [ "$scope", "ratingService",
		function($scope, ratingService) {
 
			$scope.hotelDto = {
					hotelId : null,
					hotelName : null,
					userName:null,
					 hotelName : null,
					 rating:null,
			};
		 
			this.$onInit = function () {
				ratingService.getAllHotels().then(function(value) {
					$scope.allHotels= value.data;
				}, function(error) {
					console.log(error);
				}, function(value) {
					console.log("no callback");
				});
			    };
			$scope.saveHotel = function() {
				 
				ratingService.saveHotelRating($scope.hotelDto).then(function(response) {
					console.log(" save Hotel works");
					ratingService.getAllHotels().then(function(value) {
						$scope.allHotels= value.data;
					}, function(error) {
						console.log(error);
					}, function(value) {
						console.log("no callback");
					});

				 
				}, function(reason) {
					console.log(reason);
				}, function(value) {
					console.log("no callback");
				});
			}
		} ]);