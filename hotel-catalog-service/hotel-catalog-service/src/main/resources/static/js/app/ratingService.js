'use strict'

ratingApp.factory('ratingService',
		[ "$http" , function($http) {
			var service = {};
		 
			service.getAllHotels = function() {
				return $http.get("http://localhost:8088/hotelCatalog/getAllHotels");
			}
			service.saveHotelRating = function(hotelDto) {
				return $http.post("http://localhost:8088/hotelCatalog/addHotel", hotelDto);
			}
			return service;
		} ]);