# Coding Assignment Microservices

this is a complete integrated microservices Example , it is mainly consists of three parts:
 
 * first part contains : 
	- Hotel Catalog Service (spring boot Application)
		
		this is a microservice will be responsible for :
		
		  receiving the request from front end which is (Angular App)
		  parsing the request , applying the validation and verification on it and delegate  responsibility  of handling the business to 
			the business unit which is the Hotel details service.
		  using load balancing while the delegation of the requests.
		  handling any Exception from business unit or validation Exception 
		  receiving the response from business unit , parsing it and deliver it to the end user in an appropriate way
	
 *second part contains 	
	-Hotel Details Service (spring boot Application)
		this is a microservice will be responsible for :
			receiving the request from the first microservice
			handling the business based upon the request.
			handling the connection with in-memory database (h2) for saving or retrieving  data from it.
			provide the required response to the first microservice again based upon the request
			
			

 * third part contains:
	service discovery server
	 I have used Eureka as a service discovery server where both of the two microservices are registered with in it as clients.
	 
	 
	 
since that all of these apps is running on the same server (local tomcat) so,
	 --Hotel Catalog Service is running on port: 8088
	 --Hotel Details Service is running on port: 8090
	--Eureka as the service discovery server is running on port: 8761
	-- Hystrix Dashboard http://localhost:8088/hystrix/monitor?stream=http%3A%2F%2Flocalhost%3A8088%2Factuator%2Fhystrix.stream
	

   == for running == :
	 
	 start by running the Eureka server App
	 then run the two microservices
	 use the Hotel Catalog Service home page to add and retrieve the information.
	 note:
	 logs will be in >> D:/log/hotelCatlogServiceApp.log
	 

used technologies:
	 
	 * Spring boot
	 * Spring data
	 * Spring rest
	 * Spring cloud
	 * java 8
	 * h2 in-memory database 
	 * Angular js
	 * hibernate validation
	 * slf4j.Logger for logging
	 * Eureka server
	 * hystrix 
	 * hystrix-dashboard
	 * Eurek discovery client
	 * mvn as a building tool
	 * tomcat as server
	
	 
	 